# OpenML dataset: vinnie

https://www.openml.org/d/519

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

Following are data on the shooting of Vinnie Johnson of the Detroit
Pistons during the 1985-1986 through 1988-1989 seasons. Source was the
New York Times.
The data are analyzed in the Carnegie Mellon Ph.D. Thesis of
Kate Hsiao and some results are cited in Example 2 of Kass, R.E. and
Raftery, A.E. (1995), Bayes Factors, J. Amer. Statist. Assoc.,
The first column is the year, with 85 indicating 1985-1986, etc..
The second column is Field Goals, the third column is Field Goal
Attempts.
A more complete version of the data, including free throws, is
appended together with additional information.


Information about the dataset
CLASSTYPE: numeric
CLASSINDEX: none specific

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/519) of an [OpenML dataset](https://www.openml.org/d/519). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/519/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/519/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/519/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

